# Localized version of PHP DateTime objects.

Allows one to properly format PHP `\DateTime` and `\DateTimeImmutable` objects in desired locale.

Additionaly it converts `\DateTime` and `\DateTimeImmutable` constructor exceptions to `\Rodziu\DateTimeLocalized\DateTimeException` that inherits from `\RuntimeException`.

## Prerequisites

- PHP 7.1+

## Installation

```bash
composer require rodziu/datetimelocalized
```

## Usage

```php
<?php
\Rodziu\DateTimeLocalized\Config::setLocale('pl'); // sets desired locale, full list available at src/locale directory
(new \Rodziu\DateTimeLocalized\DateTime('2018-08-01'))->format('F M D l'); // returns "Sierpień Sie Śr Środa"
(new \Rodziu\DateTimeLocalized\DateTimeImmutable('2018-08-01'))->format('F M D l'); // returns "Sierpień Sie Śr Środa"
```