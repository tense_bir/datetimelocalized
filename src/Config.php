<?php
/**
 *     ____        __     _______                __                     ___                __
 *    / __ \____ _/ /____/_  __(_)___ ___  ___  / /   ____  _________ _/ (_)___  ___  ____/ /
 *   / / / / __ `/ __/ _ \/ / / / __ `__ \/ _ \/ /   / __ \/ ___/ __ `/ / /_  / / _ \/ __  /
 *  / /_/ / /_/ / /_/  __/ / / / / / / / /  __/ /___/ /_/ / /__/ /_/ / / / / /_/  __/ /_/ /
 * /_____/\__,_/\__/\___/_/ /_/_/ /_/ /_/\___/_____/\____/\___/\__,_/_/_/ /___/\___/\__,_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017-2018.
 */

namespace Rodziu\DateTimeLocalized;

/**
 * Class Config
 */
abstract class Config{
	public const PHP_LOCALE = [
		'months'      => ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
		'days'        => ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
		'daysShort'   => ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
	];
	/**
	 * @var null|string
	 */
	private static $locale = null;

	/**
	 * @return null|string
	 */
	public static function getLocale(){
		return self::$locale;
	}

	/**
	 * @param string $locale
	 *
	 * @return array
	 */
	public static function setLocale(string $locale): array{
		$path = __DIR__.DIRECTORY_SEPARATOR.'locale'.DIRECTORY_SEPARATOR.$locale.'.php';
		if(file_exists($path)){
			/** @noinspection PhpIncludeInspection */
			return self::$locale = include $path;
		}else{
			throw new \InvalidArgumentException("No such locale `$locale`!");
		}
	}
}