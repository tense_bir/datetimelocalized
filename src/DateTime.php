<?php
/**
 *     ____        __     _______                __                     ___                __
 *    / __ \____ _/ /____/_  __(_)___ ___  ___  / /   ____  _________ _/ (_)___  ___  ____/ /
 *   / / / / __ `/ __/ _ \/ / / / __ `__ \/ _ \/ /   / __ \/ ___/ __ `/ / /_  / / _ \/ __  /
 *  / /_/ / /_/ / /_/  __/ / / / / / / / /  __/ /___/ /_/ / /__/ /_/ / / / / /_/  __/ /_/ /
 * /_____/\__,_/\__/\___/_/ /_/_/ /_/ /_/\___/_____/\____/\___/\__,_/_/_/ /___/\___/\__,_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017-2018.
 */

namespace Rodziu\DateTimeLocalized;

/**
 * Class DateTime
 * @package Rodziu\DateTimeLocalized
 */
class DateTime extends \DateTime{
	use DateTimeTrait;

	/**
	 * DateTime constructor.
	 *
	 * @param string $time
	 * @param \DateTimeZone|null $timezone
	 */
	public function __construct(string $time = 'now', \DateTimeZone $timezone = null){
		try{
			parent::__construct($time, $timezone);
		}catch(\Exception $e){
			throw new DateTimeException($e->getMessage(),$e->getCode(), $e);
		}
	}
}