<?php
/**
 *     ____        __     _______                __                     ___                __
 *    / __ \____ _/ /____/_  __(_)___ ___  ___  / /   ____  _________ _/ (_)___  ___  ____/ /
 *   / / / / __ `/ __/ _ \/ / / / __ `__ \/ _ \/ /   / __ \/ ___/ __ `/ / /_  / / _ \/ __  /
 *  / /_/ / /_/ / /_/  __/ / / / / / / / /  __/ /___/ /_/ / /__/ /_/ / / / / /_/  __/ /_/ /
 * /_____/\__,_/\__/\___/_/ /_/_/ /_/ /_/\___/_____/\____/\___/\__,_/_/_/ /___/\___/\__,_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017-2018.
 */

namespace Rodziu\DateTimeLocalized;

/**
 * Trait DateTimeTrait
 * @package Rodziu\DateTimeLocalized
 */
trait DateTimeTrait{
	/**
	 * @param string $format
	 *
	 * @return string
	 */
	public function format($format){
		$locale = Config::getLocale();
		if(is_null($locale)){
			$locale = Config::setLocale('en-gb');
		}
		$length = mb_strlen($format, 'utf-8');
		$newFormat = '';
		// F M D l - Sierpień Sie Śr Środa
		for($i = 0; $i < $length; $i++){
			$char = $format[$i];
			if(!($i > 0 && $format[$i - 1] == '\\')){
				switch($char){
					case 'F':
						$char = addcslashes($locale['months'][parent::format('n') - 1], 'A..z');
						break;
					case 'M':
						$char = addcslashes($locale['monthsShort'][parent::format('n') - 1], 'A..z');
						break;
					case 'l':
						$char = addcslashes($locale['days'][parent::format('w')], 'A..z');
						break;
					case 'D':
						$char = addcslashes($locale['daysShort'][parent::format('w')], 'A..z');
						break;
					default:
						break;
				}
			}
			$newFormat .= $char;
		}
		return parent::format($newFormat);
	}

	/**
	 * @param string $format
	 * @param string $time
	 * @param null $timezone
	 *
	 * @return mixed
	 */
	public static function createFromFormat($format, $time, $timezone = null): self{
		$locale = Config::getLocale();
		if(is_null($locale)){
			$locale = Config::setLocale('en-gb');
		}
		foreach($locale as $type => $values){
			foreach($values as $k => $value){
				$time = str_replace($value, Config::PHP_LOCALE[$type][$k], $time);
			}
		}
		return (new self())->setTimestamp(
			(parent::createFromFormat($format, $time, $timezone))->getTimestamp()
		);
	}
}