<?php
/**
 * DateTimeLocalized locale Breton [br]
 */
return [
	'months' => ['Genver', 'C\'hwevrer', 'Meurzh', 'Ebrel', 'Mae', 'Mezheven', 'Gouere', 'Eost', 'Gwengolo', 'Here', 'Du', 'Kerzu'],
	'monthsShort' => ['Gen', 'C\'hwe', 'Meu', 'Ebr', 'Mae', 'Eve', 'Gou', 'Eos', 'Gwe', 'Her', 'Du', 'Ker'],
	'days' => ['Sul', 'Lun', 'Meurzh', 'Merc\'her', 'Yaou', 'Gwener', 'Sadorn'],
	'daysShort' => ['Sul', 'Lun', 'Meu', 'Mer', 'Yao', 'Gwe', 'Sad']
];
