<?php
/**
 * DateTimeLocalized locale Malay [ms-my]
 */
return [
	'months' => ['Januari', 'Februari', 'Mac', 'April', 'Mei', 'Jun', 'Julai', 'Ogos', 'September', 'Oktober', 'November', 'Disember'],
	'monthsShort' => ['Jan', 'Feb', 'Mac', 'Apr', 'Mei', 'Jun', 'Jul', 'Ogs', 'Sep', 'Okt', 'Nov', 'Dis'],
	'days' => ['Ahad', 'Isnin', 'Selasa', 'Rabu', 'Khamis', 'Jumaat', 'Sabtu'],
	'daysShort' => ['Ahd', 'Isn', 'Sel', 'Rab', 'Kha', 'Jum', 'Sab']
];
