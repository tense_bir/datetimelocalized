<?php
/**
 * DateTimeLocalized locale Romanian [ro]
 */
return [
	'months' => ['Ianuarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie'],
	'monthsShort' => ['Ian.', 'Febr.', 'Mart.', 'Apr.', 'Mai', 'Iun.', 'Iul.', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'],
	'days' => ['Duminică', 'Luni', 'Marți', 'Miercuri', 'Joi', 'Vineri', 'Sâmbătă'],
	'daysShort' => ['Dum', 'Lun', 'Mar', 'Mie', 'Joi', 'Vin', 'Sâm']
];
