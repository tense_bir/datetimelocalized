<?php
/**
 * DateTimeLocalized locale Tagalog (Philippines) [tl-ph]
 */
return [
	'months' => ['Enero', 'Pebrero', 'Marso', 'Abril', 'Mayo', 'Hunyo', 'Hulyo', 'Agosto', 'Setyembre', 'Oktubre', 'Nobyembre', 'Disyembre'],
	'monthsShort' => ['Ene', 'Peb', 'Mar', 'Abr', 'May', 'Hun', 'Hul', 'Ago', 'Set', 'Okt', 'Nob', 'Dis'],
	'days' => ['Linggo', 'Lunes', 'Martes', 'Miyerkules', 'Huwebes', 'Biyernes', 'Sabado'],
	'daysShort' => ['Lin', 'Lun', 'Mar', 'Miy', 'Huw', 'Biy', 'Sab']
];
