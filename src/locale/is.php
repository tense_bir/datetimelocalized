<?php
/**
 * DateTimeLocalized locale Icelandic [is]
 */
return [
	'months' => ['Janúar', 'Febrúar', 'Mars', 'Apríl', 'Maí', 'Júní', 'Júlí', 'ágúst', 'September', 'Október', 'Nóvember', 'Desember'],
	'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'Maí', 'Jún', 'Júl', 'ágú', 'Sep', 'Okt', 'Nóv', 'Des'],
	'days' => ['Sunnudagur', 'Mánudagur', 'þriðjudagur', 'Miðvikudagur', 'Fimmtudagur', 'Föstudagur', 'Laugardagur'],
	'daysShort' => ['Sun', 'Mán', 'þri', 'Mið', 'Fim', 'Fös', 'Lau']
];
