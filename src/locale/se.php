<?php
/**
 * DateTimeLocalized locale Northern Sami [se]
 */
return [
	'months' => ['Ođđajagemánnu', 'Guovvamánnu', 'Njukčamánnu', 'Cuoŋománnu', 'Miessemánnu', 'Geassemánnu', 'Suoidnemánnu', 'Borgemánnu', 'čakčamánnu', 'Golggotmánnu', 'Skábmamánnu', 'Juovlamánnu'],
	'monthsShort' => ['Ođđj', 'Guov', 'Njuk', 'Cuo', 'Mies', 'Geas', 'Suoi', 'Borg', 'čakč', 'Golg', 'Skáb', 'Juov'],
	'days' => ['Sotnabeaivi', 'Vuossárga', 'Maŋŋebárga', 'Gaskavahkku', 'Duorastat', 'Bearjadat', 'Lávvardat'],
	'daysShort' => ['Sotn', 'Vuos', 'Maŋ', 'Gask', 'Duor', 'Bear', 'Láv']
];
