<?php
/**
 * DateTimeLocalized locale Pseudo [x-pseudo]
 */
return [
	'months' => ['J~áñúá~rý', 'F~ébrú~árý', '~Márc~h', 'Áp~ríl', '~Máý', '~Júñé~', 'Júl~ý', 'Áú~gúst~', 'Sép~témb~ér', 'Ó~ctób~ér', 'Ñ~óvém~bér', '~Décé~mbér'],
	'monthsShort' => ['J~áñ', '~Féb', '~Már', '~Ápr', '~Máý', '~Júñ', '~Júl', '~Áúg', '~Sép', '~Óct', '~Ñóv', '~Déc'],
	'days' => ['S~úñdá~ý', 'Mó~ñdáý~', 'Túé~sdáý~', 'Wéd~ñésd~áý', 'T~húrs~dáý', '~Fríd~áý', 'S~átúr~dáý'],
	'daysShort' => ['S~úñ', '~Móñ', '~Túé', '~Wéd', '~Thú', '~Frí', '~Sát']
];
