<?php
/**
 * DateTimeLocalized locale Faroese [fo]
 */
return [
	'months' => ['Januar', 'Februar', 'Mars', 'Apríl', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Desember'],
	'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'],
	'days' => ['Sunnudagur', 'Mánadagur', 'Týsdagur', 'Mikudagur', 'Hósdagur', 'Fríggjadagur', 'Leygardagur'],
	'daysShort' => ['Sun', 'Mán', 'Týs', 'Mik', 'Hós', 'Frí', 'Ley']
];
