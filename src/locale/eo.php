<?php
/**
 * DateTimeLocalized locale Esperanto [eo]
 */
return [
	'months' => ['Januaro', 'Februaro', 'Marto', 'Aprilo', 'Majo', 'Junio', 'Julio', 'Aŭgusto', 'Septembro', 'Oktobro', 'Novembro', 'Decembro'],
	'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aŭg', 'Sep', 'Okt', 'Nov', 'Dec'],
	'days' => ['Dimanĉo', 'Lundo', 'Mardo', 'Merkredo', 'ĵaŭdo', 'Vendredo', 'Sabato'],
	'daysShort' => ['Dim', 'Lun', 'Mard', 'Merk', 'ĵaŭ', 'Ven', 'Sab']
];
