<?php
/**
 * DateTimeLocalized locale Central Atlas Tamazight Latin [tzm-latn]
 */
return [
	'months' => ['Innayr', 'Brˤayrˤ', 'Marˤsˤ', 'Ibrir', 'Mayyw', 'Ywnyw', 'Ywlywz', 'ɣwšt', 'šwtanbir', 'Ktˤwbrˤ', 'Nwwanbir', 'Dwjnbir'],
	'monthsShort' => ['Innayr', 'Brˤayrˤ', 'Marˤsˤ', 'Ibrir', 'Mayyw', 'Ywnyw', 'Ywlywz', 'ɣwšt', 'šwtanbir', 'Ktˤwbrˤ', 'Nwwanbir', 'Dwjnbir'],
	'days' => ['Asamas', 'Aynas', 'Asinas', 'Akras', 'Akwas', 'Asimwas', 'Asiḍyas'],
	'daysShort' => ['Asamas', 'Aynas', 'Asinas', 'Akras', 'Akwas', 'Asimwas', 'Asiḍyas']
];
