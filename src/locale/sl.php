<?php
/**
 * DateTimeLocalized locale Slovenian [sl]
 */
return [
	'months' => ['Januar', 'Februar', 'Marec', 'April', 'Maj', 'Junij', 'Julij', 'Avgust', 'September', 'Oktober', 'November', 'December'],
	'monthsShort' => ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'Maj.', 'Jun.', 'Jul.', 'Avg.', 'Sep.', 'Okt.', 'Nov.', 'Dec.'],
	'days' => ['Nedelja', 'Ponedeljek', 'Torek', 'Sreda', 'četrtek', 'Petek', 'Sobota'],
	'daysShort' => ['Ned.', 'Pon.', 'Tor.', 'Sre.', 'čet.', 'Pet.', 'Sob.']
];
