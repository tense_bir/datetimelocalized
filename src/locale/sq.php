<?php
/**
 * DateTimeLocalized locale Albanian [sq]
 */
return [
	'months' => ['Janar', 'Shkurt', 'Mars', 'Prill', 'Maj', 'Qershor', 'Korrik', 'Gusht', 'Shtator', 'Tetor', 'Nëntor', 'Dhjetor'],
	'monthsShort' => ['Jan', 'Shk', 'Mar', 'Pri', 'Maj', 'Qer', 'Kor', 'Gus', 'Sht', 'Tet', 'Nën', 'Dhj'],
	'days' => ['E Diel', 'E Hënë', 'E Martë', 'E Mërkurë', 'E Enjte', 'E Premte', 'E Shtunë'],
	'daysShort' => ['Die', 'Hën', 'Mar', 'Mër', 'Enj', 'Pre', 'Sht']
];
