<?php
/**
 * DateTimeLocalized locale Croatian [hr]
 */
return [
	'months' => ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac'],
	'monthsShort' => ['Sij.', 'Velj.', 'Ožu.', 'Tra.', 'Svi.', 'Lip.', 'Srp.', 'Kol.', 'Ruj.', 'Lis.', 'Stu.', 'Pro.'],
	'days' => ['Nedjelja', 'Ponedjeljak', 'Utorak', 'Srijeda', 'četvrtak', 'Petak', 'Subota'],
	'daysShort' => ['Ned.', 'Pon.', 'Uto.', 'Sri.', 'čet.', 'Pet.', 'Sub.']
];
