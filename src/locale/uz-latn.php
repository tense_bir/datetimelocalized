<?php
/**
 * DateTimeLocalized locale Uzbek Latin [uz-latn]
 */
return [
	'months' => ['Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'Iyun', 'Iyul', 'Avgust', 'Sentabr', 'Oktabr', 'Noyabr', 'Dekabr'],
	'monthsShort' => ['Yan', 'Fev', 'Mar', 'Apr', 'May', 'Iyun', 'Iyul', 'Avg', 'Sen', 'Okt', 'Noy', 'Dek'],
	'days' => ['Yakshanba', 'Dushanba', 'Seshanba', 'Chorshanba', 'Payshanba', 'Juma', 'Shanba'],
	'daysShort' => ['Yak', 'Dush', 'Sesh', 'Chor', 'Pay', 'Jum', 'Shan']
];
