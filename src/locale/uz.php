<?php
/**
 * DateTimeLocalized locale Uzbek [uz]
 */
return [
	'months' => ['январ', 'феврал', 'март', 'апрел', 'май', 'июн', 'июл', 'август', 'сентябр', 'октябр', 'ноябр', 'декабр'],
	'monthsShort' => ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
	'days' => ['Якшанба', 'Душанба', 'Сешанба', 'Чоршанба', 'Пайшанба', 'Жума', 'Шанба'],
	'daysShort' => ['Якш', 'Душ', 'Сеш', 'Чор', 'Пай', 'Жум', 'Шан']
];
