<?php
/**
 * DateTimeLocalized locale Tetun Dili (East Timor) [tet]
 */
return [
	'months' => ['Janeiru', 'Fevereiru', 'Marsu', 'Abril', 'Maiu', 'Juniu', 'Juliu', 'Augustu', 'Setembru', 'Outubru', 'Novembru', 'Dezembru'],
	'monthsShort' => ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Aug', 'Set', 'Out', 'Nov', 'Dez'],
	'days' => ['Domingu', 'Segunda', 'Tersa', 'Kuarta', 'Kinta', 'Sexta', 'Sabadu'],
	'daysShort' => ['Dom', 'Seg', 'Ters', 'Kua', 'Kint', 'Sext', 'Sab']
];
