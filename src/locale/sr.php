<?php
/**
 * DateTimeLocalized locale Serbian [sr]
 */
return [
	'months' => ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
	'monthsShort' => ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'Maj', 'Jun', 'Jul', 'Avg.', 'Sep.', 'Okt.', 'Nov.', 'Dec.'],
	'days' => ['Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'četvrtak', 'Petak', 'Subota'],
	'daysShort' => ['Ned.', 'Pon.', 'Uto.', 'Sre.', 'čet.', 'Pet.', 'Sub.']
];
