<?php
/**
 * DateTimeLocalized locale Afrikaans [af]
 */
return [
	'months' => ['Januarie', 'Februarie', 'Maart', 'April', 'Mei', 'Junie', 'Julie', 'Augustus', 'September', 'Oktober', 'November', 'Desember'],
	'monthsShort' => ['Jan', 'Feb', 'Mrt', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'],
	'days' => ['Sondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrydag', 'Saterdag'],
	'daysShort' => ['Son', 'Maa', 'Din', 'Woe', 'Don', 'Vry', 'Sat']
];
