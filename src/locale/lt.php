<?php
/**
 * DateTimeLocalized locale Lithuanian [lt]
 */
return [
	'months' => ['Sausis', 'Vasaris', 'Kovas', 'Balandis', 'Gegužė', 'Birželis', 'Liepa', 'Rugpjūtis', 'Rugsėjis', 'Spalis', 'Lapkritis', 'Gruodis'],
	'monthsShort' => ['Sau', 'Vas', 'Kov', 'Bal', 'Geg', 'Bir', 'Lie', 'Rgp', 'Rgs', 'Spa', 'Lap', 'Grd'],
	'days' => ['Sekmadienis', 'Pirmadienis', 'Antradienis', 'Trečiadienis', 'Ketvirtadienis', 'Penktadienis', 'šeštadienis'],
	'daysShort' => ['Sek', 'Pir', 'Ant', 'Tre', 'Ket', 'Pen', 'Šeš']
];
