<?php
/**
 * DateTimeLocalized locale Frisian [fy]
 */
return [
	'months' => ['Jannewaris', 'Febrewaris', 'Maart', 'April', 'Maaie', 'Juny', 'July', 'Augustus', 'Septimber', 'Oktober', 'Novimber', 'Desimber'],
	'monthsShort' => ['Jan.', 'Feb.', 'Mrt.', 'Apr.', 'Mai', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Okt.', 'Nov.', 'Des.'],
	'days' => ['Snein', 'Moandei', 'Tiisdei', 'Woansdei', 'Tongersdei', 'Freed', 'Sneon'],
	'daysShort' => ['Si.', 'Mo.', 'Ti.', 'Wo.', 'To.', 'Fr.', 'So.']
];
