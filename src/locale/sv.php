<?php
/**
 * DateTimeLocalized locale Swedish [sv]
 */
return [
	'months' => ['Januari', 'Februari', 'Mars', 'April', 'Maj', 'Juni', 'Juli', 'Augusti', 'September', 'Oktober', 'November', 'December'],
	'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
	'days' => ['Söndag', 'Måndag', 'Tisdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lördag'],
	'daysShort' => ['Sön', 'Mån', 'Tis', 'Ons', 'Tor', 'Fre', 'Lör']
];
