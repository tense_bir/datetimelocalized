<?php
/**
 * DateTimeLocalized locale Danish [da]
 */
return [
	'months' => ['Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'December'],
	'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
	'days' => ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'],
	'daysShort' => ['Søn', 'Man', 'Tir', 'Ons', 'Tor', 'Fre', 'Lør']
];
