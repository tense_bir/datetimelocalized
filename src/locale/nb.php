<?php
/**
 * DateTimeLocalized locale Norwegian Bokmål [nb]
 */
return [
	'months' => ['Januar', 'Februar', 'Mars', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Desember'],
	'monthsShort' => ['Jan.', 'Feb.', 'Mars', 'April', 'Mai', 'Juni', 'Juli', 'Aug.', 'Sep.', 'Okt.', 'Nov.', 'Des.'],
	'days' => ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'],
	'daysShort' => ['Sø.', 'Ma.', 'Ti.', 'On.', 'To.', 'Fr.', 'Lø.']
];
