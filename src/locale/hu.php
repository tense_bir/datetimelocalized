<?php
/**
 * DateTimeLocalized locale Hungarian [hu]
 */
return [
	'months' => ['Január', 'Február', 'Március', 'április', 'Május', 'Június', 'Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
	'monthsShort' => ['Jan', 'Feb', 'Márc', 'ápr', 'Máj', 'Jún', 'Júl', 'Aug', 'Szept', 'Okt', 'Nov', 'Dec'],
	'days' => ['Vasárnap', 'Hétfő', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
	'daysShort' => ['Vas', 'Hét', 'Kedd', 'Sze', 'Csüt', 'Pén', 'Szo']
];
