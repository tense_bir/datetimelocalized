<?php
/**
 * DateTimeLocalized locale Klingon [tlh]
 */
return [
	'months' => ['Tera’ jar wa’', 'Tera’ jar cha’', 'Tera’ jar wej', 'Tera’ jar loS', 'Tera’ jar vagh', 'Tera’ jar jav', 'Tera’ jar Soch', 'Tera’ jar chorgh', 'Tera’ jar Hut', 'Tera’ jar wa’maH', 'Tera’ jar wa’maH wa’', 'Tera’ jar wa’maH cha’'],
	'monthsShort' => ['Jar wa’', 'Jar cha’', 'Jar wej', 'Jar loS', 'Jar vagh', 'Jar jav', 'Jar Soch', 'Jar chorgh', 'Jar Hut', 'Jar wa’maH', 'Jar wa’maH wa’', 'Jar wa’maH cha’'],
	'days' => ['LojmItjaj', 'DaSjaj', 'Povjaj', 'GhItlhjaj', 'Loghjaj', 'Buqjaj', 'GhInjaj'],
	'daysShort' => ['LojmItjaj', 'DaSjaj', 'Povjaj', 'GhItlhjaj', 'Loghjaj', 'Buqjaj', 'GhInjaj']
];
