<?php
/**
 * DateTimeLocalized locale Javanese [jv]
 */
return [
	'months' => ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'],
	'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nop', 'Des'],
	'days' => ['Minggu', 'Senen', 'Seloso', 'Rebu', 'Kemis', 'Jemuwah', 'Septu'],
	'daysShort' => ['Min', 'Sen', 'Sel', 'Reb', 'Kem', 'Jem', 'Sep']
];
