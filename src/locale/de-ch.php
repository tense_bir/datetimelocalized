<?php
/**
 * DateTimeLocalized locale German (Switzerland) [de-ch]
 */
return [
	'months' => ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
	'monthsShort' => ['Jan.', 'Febr.', 'März', 'April', 'Mai', 'Juni', 'Juli', 'Aug.', 'Sept.', 'Okt.', 'Nov.', 'Dez.'],
	'days' => ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
	'daysShort' => ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
];
