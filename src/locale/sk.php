<?php
/**
 * DateTimeLocalized locale Slovak [sk]
 */
return [
	'months' => ['Január', 'Február', 'Marec', 'Apríl', 'Máj', 'Jún', 'Júl', 'August', 'September', 'Október', 'November', 'December'],
	'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'Máj', 'Jún', 'Júl', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
	'days' => ['Nedeľa', 'Pondelok', 'Utorok', 'Streda', 'štvrtok', 'Piatok', 'Sobota'],
	'daysShort' => ['Ne', 'Po', 'Ut', 'St', 'št', 'Pi', 'So']
];
