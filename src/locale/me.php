<?php
/**
 * DateTimeLocalized locale Montenegrin [me]
 */
return [
	'months' => ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
	'monthsShort' => ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'Maj', 'Jun', 'Jul', 'Avg.', 'Sep.', 'Okt.', 'Nov.', 'Dec.'],
	'days' => ['Nedjelja', 'Ponedjeljak', 'Utorak', 'Srijeda', 'četvrtak', 'Petak', 'Subota'],
	'daysShort' => ['Ned.', 'Pon.', 'Uto.', 'Sri.', 'čet.', 'Pet.', 'Sub.']
];
