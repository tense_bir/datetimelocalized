<?php
/**
 * DateTimeLocalized locale Nynorsk [nn]
 */
return [
	'months' => ['Januar', 'Februar', 'Mars', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Desember'],
	'monthsShort' => ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'],
	'days' => ['Sundag', 'Måndag', 'Tysdag', 'Onsdag', 'Torsdag', 'Fredag', 'Laurdag'],
	'daysShort' => ['Sun', 'Mån', 'Tys', 'Ons', 'Tor', 'Fre', 'Lau']
];
