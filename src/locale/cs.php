<?php
/**
 * DateTimeLocalized locale Czech [cs]
 */
return [
	'months' => ['Leden', 'únor', 'Březen', 'Duben', 'Květen', 'červen', 'červenec', 'Srpen', 'Září', 'říjen', 'Listopad', 'Prosinec'],
	'monthsShort' => ['Led', 'úno', 'Bře', 'Dub', 'Kvě', 'čvn', 'čvc', 'Srp', 'Zář', 'říj', 'Lis', 'Pro'],
	'days' => ['Neděle', 'Pondělí', 'úterý', 'Středa', 'čtvrtek', 'Pátek', 'Sobota'],
	'daysShort' => ['Ne', 'Po', 'út', 'St', 'čt', 'Pá', 'So']
];
