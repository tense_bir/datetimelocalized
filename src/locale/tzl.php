<?php
/**
 * DateTimeLocalized locale Talossan [tzl]
 */
return [
	'months' => ['Januar', 'Fevraglh', 'Març', 'Avrïu', 'Mai', 'Gün', 'Julia', 'Guscht', 'Setemvar', 'Listopäts', 'Noemvar', 'Zecemvar'],
	'monthsShort' => ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Gün', 'Jul', 'Gus', 'Set', 'Lis', 'Noe', 'Zec'],
	'days' => ['Súladi', 'Lúneçi', 'Maitzi', 'Márcuri', 'Xhúadi', 'Viénerçi', 'Sáturi'],
	'daysShort' => ['Súl', 'Lún', 'Mai', 'Már', 'Xhú', 'Vié', 'Sát']
];
