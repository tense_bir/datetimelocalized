<?php
/**
 *     ____        __     _______                __                     ___                __
 *    / __ \____ _/ /____/_  __(_)___ ___  ___  / /   ____  _________ _/ (_)___  ___  ____/ /
 *   / / / / __ `/ __/ _ \/ / / / __ `__ \/ _ \/ /   / __ \/ ___/ __ `/ / /_  / / _ \/ __  /
 *  / /_/ / /_/ / /_/  __/ / / / / / / / /  __/ /___/ /_/ / /__/ /_/ / / / / /_/  __/ /_/ /
 * /_____/\__,_/\__/\___/_/ /_/_/ /_/ /_/\___/_____/\____/\___/\__,_/_/_/ /___/\___/\__,_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017-2018.
 */

namespace Rodziu\DateTimeLocalized;

use PHPUnit\Framework\TestCase;

/**
 * Class DateTimeLocalizedTest
 * @package Rodziu
 */
class DateTimeTest extends TestCase{
	/**
	 */
	protected function setUp(){
		parent::setUp();
		Config::setLocale('pl');
	}

	/**
	 */
	public function testInvalidConstruct(){
		$this->expectException(DateTimeException::class);
		new DateTime("not a date");
	}

	/**
	 */
	public function testPeriod(){
		$period = new \DatePeriod(
			new DateTime(),
			\DateInterval::createFromDateString('1 day'),
			(new \DateTime())->add(\DateInterval::createFromDateString('2 days'))
		);
		// date period should convert all dates to start date type
		self::assertInstanceOf(DateTime::class, $period->getStartDate());
		self::assertInstanceOf(DateTime::class, $period->getEndDate());
		foreach($period as $p){
			self::assertInstanceOf(DateTime::class, $p);
		}
	}

	/**
	 * @dataProvider dataProvider
	 *
	 * @param string $format
	 * @param $expected
	 */
	public function testFormat(string $format, $expected){
		$this->assertEquals($expected, (new DateTime('2017-08-02 14:01:11'))->format($format));
	}

	/**
	 * @return array
	 */
	public function dataProvider(): array{
		return [
			['\FF \a', 'FSierpień a'],
			['Y-m-d H:i:s', '2017-08-02 14:01:11'],
			['M D l', 'Sie Śr Środa']
		];
	}

	/**
	 * @dataProvider dataProvider
	 *
	 * @param string $format
	 * @param string $time
	 */
	public function testCreateFromFormat(string $format, string $time){
		$date = DateTime::createFromFormat($format, $time);
		$this->assertEquals($time, $date->format($format));
		self::assertInstanceOf(DateTime::class, $date);
	}
}
